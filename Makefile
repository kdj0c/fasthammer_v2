# for reproducible build
SOURCE_DATE_EPOCH := 0
export SOURCE_DATE_EPOCH

# Output directory
OUT := build

PYTHONS := $(wildcard *.py)
COMMON := $(wildcard Factions/*.yml) $(wildcard Common/*)

TEMPLATE := $(wildcard Template/*)
ICONS := $(wildcard icon/*)

RULES_HTML := $(OUT)/Rules.html
RULES_PDF := $(OUT)/Rules.pdf

# don't use built-in rules
.SUFFIXES:

.PHONY: builder
builder:
	@python3 faction.py
	@cd ArmyBuilder && python3 -m transcrypt -i -b -m -n ArmyBuilder.py

$(RULES_HTML): Common/rules.md $(TEMPLATE) Factions/rules.yml gen_rules.py
	@echo "Generating $@"
	@mkdir -p $(@D)
	@python3 gen_rules.py $@

$(RULES_PDF): $(RULES_HTML)
	@echo Generating $@
	@python3 html2pdf.py $< $@

rules: $(RULES_PDF)

all: builder rules

.PHONY: clean
clean:
	@echo Removing $(OUT)
	@rm -rf $(OUT)

.PHONY: indent
indent:
	@python3 indentyaml.py

.PHONY: check
check:
	@python3 indentyaml.py --dry-run
	@python3 check.py

.PHONY: post_check
post_check:
	@python3 post_check.py

CONV := Common/Conversion_helper.txt

$(CONV): scripts/gentable.py
	@./$< > $@

conv: $(CONV)
