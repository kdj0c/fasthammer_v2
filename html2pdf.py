#!/usr/bin/env python3

"""
Copyright 2019 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse
import logging

from weasyprint import HTML

logger = logging.getLogger("weasyprint")
logger.addHandler(logging.FileHandler("weasyprint.log"))

# weasyprint doesn't handle embedded svg, so replace with file uri instead
replace = [
    [
        '<svg class=icon><use href="#attack"/></svg>',
        "<img src=file:icon/attack.svg class=icon_ext>",
    ],
    [
        '<svg class=icon><use href="#piercing"/></svg>',
        "<img src=file:icon/piercing.svg class=icon_ext>",
    ],
    [
        '<svg class=icon><use href="#damage"/></svg>',
        "<img src=file:icon/damage.svg class=icon_ext>",
    ],
]


def main():
    parser = argparse.ArgumentParser(
        description="generate special rules in markdown, and convert it to html"
    )
    parser.add_argument(
        "infile", metavar="infile", type=str, help="path to the html input file"
    )
    parser.add_argument(
        "outfile", metavar="outfile", type=str, help="path the generated pdf file"
    )
    args = parser.parse_args()

    with open(args.infile) as f:
        data = f.read()
    for fr, to in replace:
        data = data.replace(fr, to)
    html = HTML(string=data)
    html.write_pdf(args.outfile)


if __name__ == "__main__":
    # execute only if run as a script
    main()
