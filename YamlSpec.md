# FastHammer, faction data Format.
All the faction database (models stats, unit composition, weapons, upgrades available, special rules, psychic powers) are in yaml format.
 
## Equipments
equipments.yml has 2 sections:
weapons and wargears

weapons are defined like this:
```yaml
Heavy Fusion Blaster:   <= weapon name
    range: 18           <= range, default to 0"= Close Combat weapon if not set
    attacks: 1d3        <= number of attacks, default to 1
    ap: 5               <= Armor piercing, default to 0
    damage: 1d6         <= damage default to 1
    special: [Fusion]   <= special rules, default to []
```
and wargears like this:
```yaml
Holy Keyboard:                 <= wargear name
    special: [Scout]           <= special rule for the model wearing it
    text: Overpowerd stuff     <= text to be seen in army builder
    cost: 1                    <= additionnal cost for additional effect in text
    weapons: [Shuriken Cannon] <= yes there is a shuriken cannon behind the keyboard that the model can use
```
## rules
rules.yml has 2 sections:
specialRules:
```yaml
Accurate: Ignore any negative modifier when Shooting.
```

a special rules may have cost calculated in cost.py (so the cost will take the model stats or weapons into account )
if it's a fixed amount, it can be added in the rulesCost section like this:
rulesCost:
```yaml
Accurate: 50
```
each model with Accurate will cost 50 pts more than the same without Accurate.

## models

This is a dict with name characteristics of the model
```
Tactical Drone:
  speed: 8
  cc: 5
  bs: 5
  defense: 5
  equipment: [2x Pulse Carbine]
  special: [Flying, Ambush, Drone]
```

speed, cc, bs and defense are obvious.
equipment is a list of default equipment. If it has the same weapon 3 times, just add "3x " before the name of the weapon.
Twin weapons are also automatically generated, it just double the attacks count (so 2d3 becomes 4d3).
special is a list of special rules defined in rules.yml

## units

simple unit with 1 model are declared like this:
```yaml
Hammerhead: self
```

unit with multiple models are more complex:
```yaml
Fire Warriors:
  - models: [Fire Warriors]
    min: 5
    max: 12
  - models: [Cadre Fireblade, Ethereal]
  - models: [Tactical Drone, Shield Drone, Marker Drone]
    max: 2
```
This unit of Fire Warriors, may have 5-12 Fire Warriors model, plus 1 Chief (either a Cadre Fireblade or an Ethereal), plus 0-2 drones between Tactic, Shield or Marker Drones.
the default for min is 0, and for max is 1, so no need to precise for the Chief.

## upgrades

This file contains the upgrade list.
upgrades are available for models (not for units)

```yaml
- models: [Devilfish, Hammerhead]
  add:
  - [2x Burst Cannon]
  - [2x Smart Missiles]
  remove: [4x Pulse Carbine]
- models: [Devilfish, Hammerhead, Piranha, Broadside, Sun Shark Bomber]
  add:
  - [Homing Missile]
  max: 2
- models: [Heavy Infantry]
  add:
  - [Laser Cannon]
  max: 1/3
```

### Upgrade
* models: (mandatory) the list of models which can take this upgrade
* add: (mandatory) a list of list of equipment that can be added
* remove: a list of equipment that must be removed (to replace some equipment)
* max: this upgrade can be taken only *max* time for all the models in the unit.
* max_model: this upgrade can be taken only *max_model* time per model in the unit.
(1/3 here means that you can take the upgrade once for every 3 models).
* text: For some complex upgrades, a text can be specified, and will be displayed in army builder

max and max_model must not be specified together for the same upgrade.