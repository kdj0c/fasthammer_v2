from .base64 import to_symbol
from .html import button, dbg, htmlAttrib, htmlClass, htmlTag, svg_icon
from .armory import Armory
from .cost import model_cost


armory = Armory()
armory.populate()


# return pretty string for duplicates weapons
def pCount(n):
    if n < 2:
        return ""
    return "{}x ".format(n)


# return pretty string for equipments
def pEqu(name, count):
    return pCount(count) + name + " " + armory.get(name).html()


# return pretty string for a list of equipments
def pEqus(equ):
    table = [count_equipment(e, 1) for e in equ]
    equs = [pEqu(name, c) for name, c in table]
    return "<br>".join(equs)


def count_equipment(equ, count):
    if " " not in equ:
        return equ, count
    mult, remainder = equ.split(" ", 1)
    if mult.endswith("x") and mult[:-1].isdigit():
        c = int(mult[:-1])
        if c > 1:
            return remainder, c * count
    return equ, count


class Upgrade:
    def __init__(self, template):
        self.add = template["add"]
        self.remove = template.get("remove", [])
        self.all = template.get("all", False)
        self.max = template.get("max", 0)
        self.max_model = template.get("max_model", 0)
        self.need = template.get("need", [])
        self.text = template["text"]
        self.cost = [0 for _ in self.add]

    def get_max(self, count):
        if self.max_model:
            if "/" in self.max_model:
                n, d = self.max_model.split("/")
                return n * count / d
            return self.max_model * count
        return self.max


class Equipments:
    def __init__(self):
        self.data = {}

    def has(self, equ, count=1):
        equ, count = count_equipment(equ, count)
        if equ in self.data:
            if self.data[equ] >= count:
                return True
        return False

    def remove(self, equ, count=1):
        equ, count = count_equipment(equ, count)
        if equ in self.data:
            rem = self.data[equ] - count
            if rem <= 0:
                self.data.pop(equ)
            else:
                self.data[equ] = rem

    def add(self, equ, count=1):
        equ, count = count_equipment(equ, count)
        if equ in self.data:
            self.data[equ] += count
        else:
            self.data[equ] = count

    def add_upgrade(self, count, up, add):
        for e in up.remove:
            self.remove(e, count)

        for e in add:
            self.add(e, count)

    def del_upgrade(self, count, up, add):
        for e in add:
            self.remove(e, count)

        for e in up.remove:
            self.add(e, count)


class Models:
    def __init__(self, count, name, template):
        listorder = [
            "speed",
            "cc",
            "bs",
            "defense",
            "hitpoints",
            "equipment",
            "special",
            "upgrades",
        ]
        self.template = {key: template[i] for i, key in enumerate(listorder)}
        self.count = count
        self.name = name
        self.equipments = self.base_equipments
        self.special = ", ".join(self.template["special"])
        self.up_template = [Upgrade(template) for template in self.template["upgrades"]]
        self.upgrades = self.base_upgrades
        self.basecost = self.cost
        self.currentcost = self.basecost

    @property
    def base_equipments(self):
        new_equipments = Equipments()
        for equ in self.template["equipment"]:
            new_equipments.add(equ, self.count)
        return new_equipments

    @property
    def base_upgrades(self):
        return [{} for _ in self.up_template]

    @property
    def equipment_desc(self):
        equs = [pEqu(name, c) for name, c in self.equipments.data.items()]
        return "<br>".join(equs)

    def increase(self):
        self.count += 1
        self.equipments = self.base_equipments
        self.upgrades = self.base_upgrades

    def decrease(self):
        self.count -= 1
        self.equipments = self.base_equipments
        self.upgrades = self.base_upgrades

    @property
    def cost(self):
        equipments = [[armory.get(name), c] for name, c in self.equipments.data.items()]
        return model_cost(self.count, self.template, equipments)

    def al_rows(self, unit_index, model_index, has_composition):
        name = self.name
        if self.count > 1:
            name += f" [{self.count}]"

        mname = htmlClass("div", "mname", name)
        columns = [
            ["speed", '"'],
            ["cc", ""],
            ["bs", ""],
            ["defense", "+"],
            ["hitpoints", ""],
        ]
        cells = [self.template[k] + s for k, s in columns]
        mcells = [htmlClass("div", "mcell", c) for c in cells]
        mrow = htmlClass("div", "mrow", "".join(mcells))
        mtable = htmlClass("div", "mtable", mrow)
        dcells = [htmlClass("div", "dcell", mname + mtable)]
        cells = [self.equipment_desc, self.special, self.cost]

        if has_composition and model_index == 0:
            cells.append(
                button(unit_index, svg_icon("composition"), "Composition", False)
            )
        else:
            key = f"{unit_index}_{model_index}"
            cells.append(button(key, svg_icon("edit"), "Edit", not self.has_upgrades))
        # Only first model in unit has delete buttons
        if model_index:
            cells.extend([""])
        else:
            cells.append(button(unit_index, svg_icon("delete"), "Delete"))
        dcells.extend([htmlClass("div", "dcell", cell) for cell in cells])
        attrib = 'class="drow" ' if unit_index % 2 else 'class="drow drow_grey" '
        attrib += 'draggable="true" '
        attrib += f'ondragstart="army_builder.drag(event,{unit_index})" '
        attrib += 'ondragover="army_builder.allowdrop(event)" '
        attrib += f'ondrop="army_builder.drop(event,{unit_index})" '
        row = htmlAttrib("div", attrib, "".join(dcells))
        return row

    @property
    def has_upgrades(self):
        return bool(self.template["upgrades"])

    @property
    def rules(self):
        ret = [r for r in self.template["special"]]
        for equ in self.equipments.data.keys():
            ret.extend(armory.get(equ).special)
        return set(ret)

    def _upgrade_cost(self, up, add, new_equipments):
        count = self.count if up.all else 1
        new_equipments.add_upgrade(count, up, add)
        equipments = [[armory.get(name), c] for name, c in new_equipments.data.items()]
        return model_cost(self.count, self.template, equipments)

    def upgrade_cost(self, up, add):
        new_equipments = Equipments()
        new_equipments.data.update(self.equipments.data)
        new_cost = self._upgrade_cost(up, add, new_equipments)
        return new_cost - self.currentcost

    def upgrade_cost_base(self, up, add):
        new_cost = self._upgrade_cost(up, add, self.base_equipments)
        return new_cost - self.basecost

    def upgrade_group(self, idx, up):
        text = htmlTag("h3", up.text)
        rows = ""
        for i, equ in enumerate(up.add):
            key = f"{idx}_{i}"
            enabled = self.can_take(idx, i)
            if enabled:
                cost = self.upgrade_cost(up, equ)
            else:
                cost = self.upgrade_cost_base(up, equ)
            count = self.upgrade_count(idx, i)
            cells = [
                pEqus(equ),
                f"{cost} pts",
                button(key, "<b>+</b>", "UpAdd", not enabled),
                str(count),
                button(key, "<b>-</b>", "UpDel", count < 1),
            ]
            hcells = [htmlClass("div", "ucell", cell) for cell in cells]
            rows += htmlClass("div", "urow", "".join(hcells))
        return text + htmlClass("div", "utable", rows)

    def upgrade_groups(self):
        self.currentcost = self.cost
        return [self.upgrade_group(i, up) for i, up in enumerate(self.up_template)]

    @property
    def upgrade_menu(self):
        title = f"{self.name}<br>{self.cost} pts"
        title = htmlClass("h2", "up_title_item", title)
        desc = htmlClass("div", "up_title_item", self.equipment_desc)
        done = htmlClass("div", "up_title_item", button(0, "Done", "Up_Done"))
        title = htmlClass("div", "up_title", title + "\n" + desc + done)
        tables = [
            htmlClass("div", "up_group", group) for group in self.upgrade_groups()
        ]
        return title + htmlClass("div", "up_menu", "".join(tables))

    def upgrade_count(self, gidx, idx):
        ug = self.upgrades[gidx]
        if idx in ug:
            return ug[idx]
        return 0

    def upgrade_group_count(self, gidx):
        ug = self.upgrades[gidx]
        return sum([int(v) for v in ug.values()])

    def load_upgrade(self, count, upidx):
        upgidx = 0
        for gidx, template in enumerate(self.up_template):
            if upidx >= upgidx + len(template.add):
                upgidx += len(template.add)
                continue
            self.add_upgrade(gidx, upidx - upgidx, count)
            return

    def add_upgrade(self, gidx, idx, count):
        ug = self.upgrades[gidx]
        if idx in ug:
            ug[idx] = ug[idx] + count
        else:
            ug[idx] = count
        up = self.up_template[gidx]
        if up.all:
            count *= self.count

        self.equipments.add_upgrade(count, up, up.add[idx])

    def del_upgrade(self, gidx, idx):
        ug = self.upgrades[gidx]
        ug[idx] = ug[idx] - 1

        if ug[idx] == 0:
            ug.pop(idx)

        up = self.up_template[gidx]
        count = self.count if up.all else 1

        self.equipments.del_upgrade(count, up, up.add[idx])

    def can_take(self, gidx, idx):
        up = self.up_template[gidx]
        count = self.count if up.all else 1

        for equ in up.remove:
            if not self.equipments.has(equ, count):
                return False

        group_count = self.upgrade_group_count(gidx)
        max = up.get_max(self.count)
        if max > 0:
            if group_count >= int(max):
                return False
        if max == -1 and not up.remove:
            if self.upgrade_count(gidx, idx) >= 1:
                return False
        for equ in up.add[idx]:
            if hasattr(armory.get(equ), "text"):
                if self.upgrade_count(gidx, idx) >= self.count:
                    return False
        return True

    @property
    def symbols(self):
        symbols = []
        upidx = 0
        for gidx, ug in enumerate(self.upgrades):
            for idx, count in ug.items():
                if count > 1:
                    symbols.append(to_symbol(count, "count"))
                symbols.append(to_symbol(upidx + int(idx), "upgrade"))
            upidx = upidx + len(self.up_template[gidx].add)
        dbg(JSON.stringify(symbols))
        return symbols


class ModelList:
    def __init__(self, config, barracks):
        self.min = config.get("min", 0)
        self.max = config.get("max", 1)
        self.mlid = 0
        self.available_models = config.get("models")
        self.templates = {model: barracks[model] for model in self.available_models}
        self.models = {}

    def base_models(self):
        if self.min > 0:
            name = self.available_models[0]
            self.models = {name: Models(self.min, name, self.templates[name])}

    @property
    def full(self):
        return bool(self.count >= self.max)

    @property
    def count(self):
        return sum([m.count for m in self.models.values()])

    def model(self, idx):
        name = self.available_models[idx]
        return self.models[name]

    def count_model(self, name):
        if name in self.models:
            return self.models[name].count
        return 0

    def composition_rows(self, index):
        rows = ""
        for i, am in enumerate(self.available_models):
            key = f"{index}_{i}"
            count = self.count_model(am)
            disable_del = count == 0 or self.count == self.min
            disable_edit = count == 0 or not self.model(i).has_upgrades
            cells = [
                am,
                f"{self.min} - {self.max}",
                button(key, "<b>+</b>", "CompAdd", self.full),
                count,
                button(key, "<b>-</b>", "CompDel", disable_del),
                button(key, svg_icon("edit"), "UpEdit", disable_edit),
            ]
            hcells = [htmlClass("div", "ucell", cell) for cell in cells]
            rows += htmlClass("div", "urow", "".join(hcells))
        return rows

    def load(self, count, index):
        name = self.available_models[index]
        self.models[name] = Models(count, name, self.templates[name])
        return self.models[name]

    def increase(self, index):
        name = self.available_models[index]
        if name in self.models:
            self.models[name].increase()
        else:
            self.models[name] = Models(1, name, self.templates[name])

    def decrease(self, index):
        name = self.available_models[index]
        if self.models[name].count == 1:
            self.models.pop(name)
        else:
            self.models[name].decrease()

    @property
    def symbols(self):
        symbols = []
        for name, model in self.models.items():
            if model.count > 1:
                symbols.append(to_symbol(model.count, "count"))
            symbols.append(
                to_symbol(self.mlid + self.available_models.index(name), "model")
            )
            symbols.extend(model.symbols)
        dbg(JSON.stringify(symbols))
        return symbols


class Unit:
    def __init__(self, name, template, barracks, fid, uid):
        self.name = name
        if template == "self":
            self.template = [{"models": [name], "min": 1}]
        else:
            self.template = template
        self.fid = fid
        self.uid = uid
        self.barracks = barracks
        self.model_list = [ModelList(config, barracks) for config in self.template]
        mlid = 0
        for ml in self.model_list:
            ml.mlid = mlid
            mlid = mlid + len(ml.available_models)

    @property
    def has_composition(self):
        if len(self.model_list) == 1:
            ml = self.model_list[0]
            if len(ml.available_models) == 1 and ml.min == ml.max:
                return False
        return True

    def base_models(self):
        for ml in self.model_list:
            ml.base_models()

    def load_model(self, count, mid):
        for ml in self.model_list:
            if mid >= ml.mlid + len(ml.available_models):
                continue
            return ml.load(count, mid - ml.mlid)
        dbg("load model error {} {} {} {}", count, mid, self.fid, self.name)

    @property
    def models(self):
        return [m for ml in self.model_list for m in ml.models.values()]

    def al_rows(self, unit_index):
        return [
            m.al_rows(unit_index, model_index, self.has_composition)
            for model_index, m in enumerate(self.models)
        ]

    def model(self, ml_idx, m_idx):
        return self.model_list[ml_idx].model(m_idx)

    @property
    def composition_table(self):
        return htmlClass(
            "div",
            "utable",
            "".join([ml.composition_rows(i) for i, ml in enumerate(self.model_list)]),
        )

    @property
    def composition_menu(self):
        title = f"{self.name} {self.cost} pts"
        title = htmlClass("h2", "up_title_item", title)
        done = htmlClass("div", "up_title_item", button(0, "Done", "Up_Done"))
        title = htmlClass("div", "up_title", title + done)
        return title + htmlClass("div", "up_menu", self.composition_table)

    @property
    def cost(self):
        return sum([m.cost for m in self.models])

    @property
    def rules(self):
        return [r for m in self.models for r in m.rules]

    @property
    def symbols(self):
        symbols = [to_symbol(self.uid, "unit")]
        for ml in self.model_list:
            symbols.extend(ml.symbols)
        return symbols
