from .data import armory
from .html import htmlClass, htmlTag, svg_icon
from .cost import weapon_cost, combo_cost, wargear_cost


# Double the dice Value
# 1d3 => 2d3
def double_dice(value):
    if "d" not in str(value):
        return 2 * value
    n, rem = value.lower().split("d")
    n = 2 * int(n)
    return f"{n}d{rem}"


class NotFound:
    def html(self):
        return "Not Found"


class Armory:
    def __init__(self):
        self.data = {}

    def populate(self):
        for w, data in armory["weapons"].items():
            self.data[w] = Weapon(w, data)
        for w, data in armory["combos"].items():
            self.data[w] = Combo(w, data)
        for w, data in armory["wargears"].items():
            self.data[w] = Wargear(w, data)

    def get(self, name):
        if name in self.data:
            return self.data[name]
        if name.startswith("Twin "):
            wname = name[5:]
            if wname in self.data:
                self.data[name] = self.data[wname].create_twin()
                return self.data[name]
        return NotFound()


class Weapon:
    def __init__(self, name, data):
        self.name = name
        self.range, self.attacks, self.ap, self.damage, self.special = data

    def create_twin(self):
        return Weapon(
            "Twin " + self.name,
            [self.range, double_dice(self.attacks), self.ap, self.damage, self.special],
        )

    def hcore(self):
        range = f'{self.range}"' if self.range else ""
        at = svg_icon("attack") + f"{self.attacks}"
        ap = svg_icon("piercing") + f"{self.ap}" if self.ap else ""
        damage = svg_icon("damage") + f"{self.damage}" if self.damage != 1 else ""
        return [c for c in [range, at, ap, damage] if c]

    def hspecial(self):
        return [r.replace(" ", "&nbsp;") for r in self.special]

    def html(self):
        core = htmlClass("span", "nobr", ", ".join(self.hcore()))
        if len(self.hspecial()) > 0:
            core = core + ", " + ", ".join(self.hspecial())
        return f"({core})"

    def cost(self, model):
        return weapon_cost(model, self)


class Combo:
    def __init__(self, name, data):
        self.name = name
        self.profiles = {}
        for prof, pdata in data.items():
            w = Weapon(prof, pdata)
            self.profiles[prof] = w

    def create_twin(self):
        twin = Combo("Twin " + self.name, {})
        for prof, weapon in self.profiles.items():
            twin.profiles[prof] = weapon.create_twin()
        return twin

    def html(self):
        s = []
        for profile, weapon in self.profiles.items():
            html = htmlTag("i", profile) + "(" + ", ".join(weapon.hcore())
            html = htmlClass("span", "nobr", html)
            if len(weapon.special) > 0:
                html = html + ", " + ", ".join(weapon.hspecial())
            s.append(html + ")")
        return " / ".join(s)

    @property
    def special(self):
        return list({s for sps in self.profiles.values() for s in sps.special})

    def cost(self, model):
        return combo_cost(model, self)


class Wargear:
    def __init__(self, name, data):
        self.name = name
        self.text = data.get("text")
        self.rules = data.get("special", [])
        self.add_cost = data.get("cost", 0)

    @property
    def special(self):
        return self.rules

    def html(self):
        if self.text:
            return f"({self.text})"
        return "(" + ", ".join(self.rules) + ")"

    def cost(self, model):
        return wargear_cost(model, self)
